//Copyright SoftwareByNumbers Ltd 2021

//! A homogeneous tree with a visitor.
//!
//! This crate contains a tree structure with an associated visitor that
//! supports a closure over external state.  This visitor can be used to
//! traverse the tree structure, accumulating data from the branches.  For
//! example, as a parser.
//!
//! 
//!
//! # Example
//! ```rust
//!use std::fmt;
//!use patterns::{ArborealBranchType, Vis, Visitable};
//! 
//!#[derive(Debug)]
//!pub struct PersonType {
//!    age: i8,
//!    title: String,
//!    first_name: String,
//!    last_name: String,
//!    notes: String,
//!}
//!impl fmt::Display for PersonType {
//!    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//!        write!(
//!            f,
//!            r#"Title: {}\\n Name : {}, {}\\n, Age: {}\\n Patient Notes: {}\\n "#,
//!            self.title, self.last_name, self.first_name, self.age, self.notes
//!        )
//!    }
//!}
//!let test_person_1 = PersonType {
//!       age: 22,
//!       title: String::from("Mr"),
//!       first_name: String::from("Fred"),
//!       last_name: String::from("Smith"),
//!       notes: String::from("Fred Smith has got a lot better."),
//!   };
//!
//!   let expected_result =
//!      String::from("Mr Fred Smith\nAge: 22\nNotes: Fred Smith has got a lot better.\n");
//!
//!   let mut state: String = String::new();
//!
//!   let state_closure = |person: &PersonType| {
//!       state.push_str(&format!(
//!          "{} {} {}\nAge: {}\nNotes: {}\n",
//!          person.title, person.first_name, person.last_name, person.age, person.notes));
//!          println!("\nPatient Record");
//!          println!("==============\n");
//!          println!("{} {} {}\nAge: {}\nNotes: {}\n",
//!                   person.title, person.first_name, person.last_name, person.age, person.notes)};
//!   let b = ArborealBranchType::<PersonType>::branch_from_item(test_person_1);
//!   let mut v = Vis::new(state_closure);
//!   b.accept(&mut v);
//!   assert_eq!(expected_result, state);
//!
//!```
//! Run test like
//! ```text
//! cargo test -- --nocapture
//! ```
//! Test output should contain the following __however at the moment no capture is not supported in
//! doc tests__.
//! ```text
//!    Patient Record
//!    ==============
//!
//!     Mr Fred Smith
//!     Age: 22
//!     Notes: Fred Smith has got a lot better.
//!
//!```
//!
//!
//!

use contracts::requires;

/// A arboreal branch is a vector of leaves of type T, which may be empty.
pub struct ArborealBranchType<T> {
    pub value: Option<T>,
    pub leafs: Option<Vec<ArborealBranchType<T>>>,
}

impl<T> Default for ArborealBranchType<T> {
    ///Make an empty branch
    fn default() -> ArborealBranchType<T> {
        ArborealBranchType::<T> {
            value: None,
            leafs: Some(Vec::<ArborealBranchType<T>>::new()),
        }
    }
}

//Builder operations to help create a new tree.
impl<T> ArborealBranchType<T> {

    //Add/replace a value on the current branch
    fn add_value<'a>(&'a mut self, value: T) -> &'a mut ArborealBranchType<T> {
        self.value = Some(value);
        self
    }

    //Add a subtree, one for each value where each value becomes a value on a child branch.
    fn add_subtrees<'a>(&'a mut self, values: Vec<T>) -> &'a mut ArborealBranchType<T> {
        let mut leafs: Vec<ArborealBranchType<T>> = Vec::new();
        for t in values {
            leafs.push(ArborealBranchType::<T>::branch_from_item(t));
        }
        self.leafs = Some(leafs);
        self
    }

    //Add a subtree of arboreal branches
    fn add_arboreal_subtrees<'a>(&'a mut self, values: Vec<ArborealBranchType<T>>) -> &'a mut ArborealBranchType<T> {
        self.leafs = Some(values);
        self
    }
}

impl<'a, T> ArborealBranchType<T> {
    #[deprecated(since = "0.0.1", note = "Please use the value function instead.")]
    pub fn get_val(&self) -> Option<&T> {
        self.value.as_ref()
    }

    /// For this branch get the associated value
    pub fn value(&self) -> Option<&T> {
        self.value.as_ref()
    }

    ///Borrow the children of this branch
    pub fn children(&self) -> &Option<Vec<ArborealBranchType<T>>> {
        &self.leafs
    }

    ///An empty branch, no node value, no children
    pub fn new() -> ArborealBranchType<T> {
        ArborealBranchType::<T> {
            value: None,
            leafs: Some(Vec::<ArborealBranchType<T>>::new()),
        }
    }

    ///A branch with a node value but no children
    pub fn branch_from_item(item: T) -> ArborealBranchType<T> {
        ArborealBranchType::<T> {
            value: Some(item),
            leafs: Some(Vec::<ArborealBranchType<T>>::new()),
        }
    }

    ///A branch from items with empty children but with a value
    pub fn branch_from_item_empty_children(item: T) -> ArborealBranchType<T> {
        ArborealBranchType::<T> {
            value: Some(item),
            leafs: Some(Vec::<ArborealBranchType<T>>::new()),
        }
    }

    //a branch with a node value and aboreal children
    #[requires(!children.is_empty(), "We expect there to be some children")]
    pub fn branch_from_item_and_items(
        item: T,
        children: Vec<ArborealBranchType<T>>,
    ) -> ArborealBranchType<T> {
        let mut c: Vec<ArborealBranchType<T>> = vec![];
        for b in children {
            c.push(b)
        }
        ArborealBranchType::<T> {
            value: Some(item),
            leafs: Some(c),
        }
    }

    //a branch with no node value and aboreal children
    #[requires(!children.is_empty(), "We expect there to be some children")]
    pub fn branch_from_items(children: Vec<ArborealBranchType<T>>) -> ArborealBranchType<T> {
        let mut c: Vec<ArborealBranchType<T>> = vec![];
        for b in children {
            c.push(b)
        }
        ArborealBranchType::<T> {
            value: None,
            leafs: Some(c),
        }
    }
}

/// A visitor.
pub trait Visitor<T> {
    fn visit(&mut self, t: &T);
}

/// A visitable
/// To be able to visit a struct it must implement Visitable.
pub trait Visitable: Sized {
    fn accept<T>(&self, t: &mut T)
    where
        T: Visitor<Self>,
    {
        t.visit(self);
    }
}

impl<T> Visitable for ArborealBranchType<T> {}

/// A visitor closing over a no parameter function
pub struct Vis0<F>
where
    F: FnMut(),
{
    applicable: F,
}

/// A visitor closing over a single parameter function
pub struct Vis<'a, T, F>
where
    F: FnMut(&T),
{
    applicable: F,
    //This is to make the compiler work!!
    //Without it the compiler complains about the struct type parameter
    //Seems to be something to do with the closure type.
    //rustc --version
    //rustc 1.46.0 (04488afe3 2020-08-24)
    a: Vec<&'a T>,
}

/// A visitor closing over a two parameter function
pub struct Vis2<'a, T, F>
where
    F: FnMut(&T, bool),
{
    applicable: F,
    //This is to make the compiler work!!
    //Without it the compiler complains about the struct type parameter
    //Seems to be something to do with the closure type.
    //rustc --version
    //rustc 1.46.0 (04488afe3 2020-08-24)
    a: Vec<&'a T>,
}

impl<F> Vis0<F>
where
    F: FnMut(),
{
    pub fn new(applicator: F) -> Vis0<F> {
        Vis0 {
            applicable: applicator,
        }
    }
}

impl<'a, T, F> Vis<'a, T, F>
where
    F: FnMut(&T),
{
    pub fn new(applicator: F) -> Vis<'a, T, F> {
        Vis {
            applicable: applicator,
            a: vec![],
        }
    }
}

impl<'a, T, F> Vis2<'a, T, F>
where
    F: FnMut(&T, bool),
{
    pub fn new(applicator: F) -> Vis2<'a, T, F> {
        Vis2 {
            applicable: applicator,
            a: vec![],
        }
    }
}

//A displayable visitor over a no parameter function
impl<T, F> Visitor<ArborealBranchType<T>> for Vis0<F>
where
    T: std::fmt::Display,
    F: FnMut(),
{
    fn visit(&mut self, node: &ArborealBranchType<T>) {
        match node.value() {
            Some(_) => {
                (self.applicable)();
            }

            None => {
                println!("No leaf value");
            }
        }

        match node.children() {
            Some(_) => {
                let c = node.children().as_ref().unwrap();
                for n in c.iter() {
                    n.accept(self);
                }
            }
            None => println!("No children."),
        }
    }
}

//A displayable visitor over a single parameter function
impl<'a, T, F> Visitor<ArborealBranchType<T>> for Vis<'a, T, F>
where
    T: std::fmt::Display,
    F: FnMut(&T),
{
    fn visit(&mut self, node: &ArborealBranchType<T>) {
        match node.value() {
            Some(n) => {
                (self.applicable)(&n);
            }

            None => {
                println!("No leaf value");
            }
        }

        match node.children() {
            Some(_) => {
                let c = node.children().as_ref().unwrap();
                for n in c.iter() {
                    n.accept(self);
                }
            }
            None => println!("No children."),
        }
    }
}

//A displayable visitor closing over a two parameter function
impl<'a, T, F> Visitor<ArborealBranchType<T>> for Vis2<'a, T, F>
where
    T: std::fmt::Display,
    F: FnMut(&T, bool),
{
    fn visit(&mut self, node: &ArborealBranchType<T>) {
        let has_children =
            node.children().is_some() && !node.children().as_ref().unwrap().is_empty();

        match node.value() {
            Some(n) => {
                (self.applicable)(&n, has_children);
            }

            None => {
                println!("No leaf value");
            }
        }

        match node.children() {
            Some(_) => {
                let c = node.children().as_ref().unwrap();
                for n in c.iter() {
                    n.accept(self);
                }
            }
            None => println!("No children."),
        }
    }
}

//Unit tests
#[cfg(test)]
mod branch_tests {

    use super::Visitable;
    use std::fmt;

    //A person struct to populate the tree
    #[derive(Debug)]
    pub struct PersonType {
        age: i8,
        title: String,
        first_name: String,
        last_name: String,
        notes: String,
    }

    impl fmt::Display for PersonType {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(
                f,
                r#"Title: {}\\n Name : {}, {}\\n, Age: {}\\n Patient Notes: {}\\n "#,
                self.title, self.last_name, self.first_name, self.age, self.notes
            )
        }
    }



    #[test]
    fn test_person_aboreal_branch() {
        let test_person_1 = PersonType {
            age: 22,
            title: String::from("Mr"),
            first_name: String::from("Fred"),
            last_name: String::from("Smith"),
            notes: String::from("Fred Smith has got a lot better."),
        };

        let expected_result =
            String::from("Mr Fred Smith\nAge: 22\nNotes: Fred Smith has got a lot better.\n");

        let mut state: String = String::new();

        let state_closure = |person: &PersonType| {
            state.push_str(&format!(
                "{} {} {}\nAge: {}\nNotes: {}\n",
                person.title, person.first_name, person.last_name, person.age, person.notes
            ));
        };

        let print_closure = |person: &PersonType| {
            println!("\nPatient Record (Print Closure)");
            println!("==============\n");
            println!("{} {} {}\nAge: {}\nNotes: {}\n",
                     person.title, person.first_name, person.last_name, person.age, person.notes);
        };

        let b = super::ArborealBranchType::<PersonType>::branch_from_item(test_person_1);
        let mut v = super::Vis::new(state_closure);
        b.accept(&mut v);
        assert_eq!(expected_result, state);
        let mut v1 = super::Vis::new(print_closure);
        b.accept(&mut v1);
    }

    #[test]
    fn test_simple_builder() {
        let n1 = "Fred Smith".to_string();
        let n2 = "John Smith".to_string();
        let n3 = "Mabel Smith".to_string();
        let s = vec!(n1, n2, n3);
        let mut b = super::ArborealBranchType::<String>::new();
        b.add_subtrees(s);
        let mut state: String = String::new();
        let expected_result = String::from("Fred Smith::John Smith::Mabel Smith::");
        let state_closure = |s: &String| {
            state.push_str(&format!("{}::", s));
        };
        let mut v = super::Vis::new(state_closure);
        b.accept(&mut v);
        assert_eq!(expected_result, state);

        let print_closure = |name: &String| {
            println!("\nPerson Name (Print Closure)");
            println!("==============\n");
            println!("{}\n", name);
        };
        let mut c = super::Vis::new(print_closure);
        b.accept(&mut c);

    }

    #[test]
    fn test_builder() {

        let test_person_1 = PersonType {
            age: 22,
            title: String::from("Mr"),
            first_name: String::from("Fred"),
            last_name: String::from("Smith"),
            notes: String::from("Fred Smith has got a lot better."),
        };

        let test_person_2 = PersonType {
            age: 33,
            title: String::from("Ms"),
            first_name: String::from("Nora"),
            last_name: String::from("Jones"),
            notes: String::from("Nora Jones is getting better."),
        };

        let test_person_3 = PersonType {
            age: 33,
            title: String::from("Mr"),
            first_name: String::from("Sid"),
            last_name: String::from("Vicious"),
            notes: String::from("Sid Vicious is a bad man."),
        };

        let expected_result = String::from("Mr Fred Smith\nAge: 22\nNotes: Fred Smith has got a lot better.\nMs Nora Jones\nAge: 33\nNotes: Nora Jones is getting better.\nMr Sid Vicious\nAge: 33\nNotes: Sid Vicious is a bad man.\n");


        let mut state: String = String::new();

        let state_closure = |person: &PersonType| {
            state.push_str(&format!(
                            "{} {} {}\nAge: {}\nNotes: {}\n",
                            person.title, person.first_name, person.last_name, person.age, person.notes));
        };

        let print_closure = |person: &PersonType| {
            println!("\nPatient Record (Print Closure)");
            println!("==============\n");
            println!("{} {} {}\nAge: {}\nNotes: {}\n",
                     person.title, person.first_name, person.last_name, person.age, person.notes);
        };

        let a = vec!(test_person_2, test_person_3);
        let mut b = super::ArborealBranchType::<PersonType>::new();
        //Builder here
        b.add_value(test_person_1).add_subtrees(a);
        let mut v = super::Vis::new(state_closure);
        b.accept(&mut v);
        assert_eq!(expected_result, state);
        let mut v1 = super::Vis::new(print_closure);
        b.accept(&mut v1);
    }



    #[test]
    fn test_persons_aboreal_branch() {
        let test_person_1 = PersonType {
            age: 22,
            title: String::from("Mr"),
            first_name: String::from("Fred"),
            last_name: String::from("Smith"),
            notes: String::from("Fred Smith has got a lot better."),
        };

        let test_person_2 = PersonType {
            age: 33,
            title: String::from("Ms"),
            first_name: String::from("Nora"),
            last_name: String::from("Jones"),
            notes: String::from("Nora Jones is getting better."),
        };

        let test_person_3 = PersonType {
            age: 33,
            title: String::from("Mr"),
            first_name: String::from("Sid"),
            last_name: String::from("Vicious"),
            notes: String::from("Sid Vicious is a bad man."),
        };

        let expected_result = String::from("Mr Fred Smith\nAge: 22\nNotes: Fred Smith has got a lot better.\nMs Nora Jones\nAge: 33\nNotes: Nora Jones is getting better.\nMr Sid Vicious\nAge: 33\nNotes: Sid Vicious is a bad man.\n");

        let mut state: String = String::new();

        let state_closure = |person: &PersonType| {
            state.push_str(&format!(
                "{} {} {}\nAge: {}\nNotes: {}\n",
                person.title, person.first_name, person.last_name, person.age, person.notes
            ));
        };
        let print_closure = |person: &PersonType| {
            println!("\nPatient Record (Print Closure)");
            println!("==============\n");
            println!("{} {} {}\nAge: {}\nNotes: {}\n",
                     person.title, person.first_name, person.last_name, person.age, person.notes);
        };

        let a = vec![
            super::ArborealBranchType::<PersonType>::branch_from_item(test_person_2),
            super::ArborealBranchType::<PersonType>::branch_from_item(test_person_3),
        ];
        let b =
            super::ArborealBranchType::<PersonType>::branch_from_item_and_items(test_person_1, a);
        let mut v = super::Vis::new(state_closure);
        b.accept(&mut v);
        assert_eq!(expected_result, state);
        let mut v1 = super::Vis::new(print_closure);
        b.accept(&mut v1);
    }

    #[test]
    fn test_2_aboreal_branch_no_node_value() {
        //NOTE In this test the top level does have children but no node value so the visitor ignores it
        //The child itself has a node value and no children, this gets processed by the visitor.
        let test_person_1 = PersonType {
            age: 22,
            title: String::from("Mr"),
            first_name: String::from("Fred"),
            last_name: String::from("Smith"),
            notes: String::from("Fred Smith has got a lot better."),
        };

        let expected_result = String::from("Mr Fred Smith\nAge: 22\nAny children?: false\nNotes: Fred Smith has got a lot better.\n");

        let mut state: String = String::new();

        let state_closure = |person: &PersonType, has_children: bool| {
            state.push_str(&format!(
                "{} {} {}\nAge: {}\nAny children?: {}\nNotes: {}\n",
                person.title,
                person.first_name,
                person.last_name,
                person.age,
                has_children,
                person.notes
            ));
        };

        let print_closure = |person: &PersonType| {
            println!("\nPatient Record (Print Closure)");
            println!("==============\n");
            println!("{} {} {}\nAge: {}\nNotes: {}\n",
                     person.title, person.first_name, person.last_name, person.age, person.notes);
        };

        let a = vec![super::ArborealBranchType::<PersonType>::branch_from_item(
            test_person_1,
        )];
        let b = super::ArborealBranchType::<PersonType>::branch_from_items(a);

        let mut v = super::Vis2::new(state_closure);
        b.accept(&mut v);
        assert_eq!(expected_result, state);
        let mut v1 = super::Vis::new(print_closure);
        b.accept(&mut v1);
    }

    #[test]
    fn test_persons_age_accumulator() {
        //adds a up the age value from every node.

        let test_person_1 = PersonType {
            age: 22,
            title: String::from("Mr"),
            first_name: String::from("Fred"),
            last_name: String::from("Smith"),
            notes: String::from("Fred Smith has got a lot better."),
        };

        let test_person_2 = PersonType {
            age: 33,
            title: String::from("Ms"),
            first_name: String::from("Nora"),
            last_name: String::from("Jones"),
            notes: String::from("Nora Jones is getting better."),
        };

        let test_person_3 = PersonType {
            age: 33,
            title: String::from("Mr"),
            first_name: String::from("Sid"),
            last_name: String::from("Vicious"),
            notes: String::from("Sid Vicious is a bad man."),
        };

        let expected_result = 88;

        let mut state: i8 = 0;

        let state_closure = |person: &PersonType| {
            state += person.age;
        };

        let a = vec![
            super::ArborealBranchType::<PersonType>::branch_from_item(test_person_2),
            super::ArborealBranchType::<PersonType>::branch_from_item(test_person_3),
        ];
        let b =
            super::ArborealBranchType::<PersonType>::branch_from_item_and_items(test_person_1, a);
        let mut v = super::Vis::new(state_closure);
        b.accept(&mut v);
        assert_eq!(expected_result, state);
    }

    #[test]
    fn test_persons_zero_param_visitor() {
        //Counts the number of nodes in the tree

        let test_person_1 = PersonType {
            age: 22,
            title: String::from("Mr"),
            first_name: String::from("Fred"),
            last_name: String::from("Smith"),
            notes: String::from("Fred Smith has got a lot better."),
        };

        let test_person_2 = PersonType {
            age: 33,
            title: String::from("Ms"),
            first_name: String::from("Nora"),
            last_name: String::from("Jones"),
            notes: String::from("Nora Jones is getting better."),
        };

        let test_person_3 = PersonType {
            age: 33,
            title: String::from("Mr"),
            first_name: String::from("Sid"),
            last_name: String::from("Vicious"),
            notes: String::from("Sid Vicious is a bad man."),
        };

        let expected_result = 3;

        let mut state: i8 = 0;

        let state_closure = || {
            state += 1;
        };

        let a = vec![
            super::ArborealBranchType::<PersonType>::branch_from_item(test_person_2),
            super::ArborealBranchType::<PersonType>::branch_from_item(test_person_3),
        ];
        let b =
            super::ArborealBranchType::<PersonType>::branch_from_item_and_items(test_person_1, a);
        let mut v = super::Vis0::new(state_closure);
        b.accept(&mut v);
        assert_eq!(expected_result, state);
    }

    // We cannot process sub trees because they are moved into their parents and no clone
    // or copy implemented on branch yet so we try and identify sub trees by a characteristic.
    // ie is this a family tree because we found Father or Mother?
    #[test]
    fn test_subtree_age_accumulator() {
        //adds a up the age value from every node of a sub tree of the tree

        let test_person_1 = PersonType {
            age: 80,
            title: String::from("Mr"),
            first_name: String::from("Simon"),
            last_name: String::from("Smith"),
            notes: String::from("Grand Father"),
        };

        let test_person_2 = PersonType {
            age: 78,
            title: String::from("Mrs"),
            first_name: String::from("Doris"),
            last_name: String::from("Smith"),
            notes: String::from("Grand Mother"),
        };

        let test_person_3 = PersonType {
            age: 44,
            title: String::from("Mr"),
            first_name: String::from("Fred"),
            last_name: String::from("Smith"),
            notes: String::from("Father"),
        };

        let test_person_4 = PersonType {
            age: 42,
            title: String::from("Ms"),
            first_name: String::from("Saharh"),
            last_name: String::from("Smith"),
            notes: String::from("Mother"),
        };

        let test_person_5 = PersonType {
            age: 12,
            title: String::from("Ms"),
            first_name: String::from("G"),
            last_name: String::from("Smith"),
            notes: String::from("Daughter"),
        };

        let test_person_6 = PersonType {
            age: 10,
            title: String::from("Mr"),
            first_name: String::from("M"),
            last_name: String::from("Smith"),
            notes: String::from("Son"),
        };

        //combined age of the children
        let expected_childrens_age = 22;

        //combined ages of the family tree
        let expected_family_age = 108;

        //combined age of the grand family
        let expected_grand_family_age = 266;

        let children = vec![
            super::ArborealBranchType::<PersonType>::branch_from_item(test_person_5),
            super::ArborealBranchType::<PersonType>::branch_from_item(test_person_6),
        ];
        let children_persons_tree =
            super::ArborealBranchType::<PersonType>::branch_from_items(children);

        let parents_persons = vec![
            super::ArborealBranchType::<PersonType>::branch_from_item(test_person_3),
            super::ArborealBranchType::<PersonType>::branch_from_item(test_person_4),
            children_persons_tree,
        ];
        let parent_persons_tree =
            super::ArborealBranchType::<PersonType>::branch_from_items(parents_persons);

        let grand_parents_persons = vec![
            super::ArborealBranchType::<PersonType>::branch_from_item(test_person_1),
            super::ArborealBranchType::<PersonType>::branch_from_item(test_person_2),
            parent_persons_tree,
        ];

        let grand_parents_persons_tree =
            super::ArborealBranchType::<PersonType>::branch_from_items(grand_parents_persons);

        //The children tree age
        let mut childrens_age: i16 = 0;
        let mut children_tree_found = false;
        let children_tree_finder = |person: &PersonType| {
            if person.notes == String::from("Daughter")
                || person.notes == String::from("Son")
                || children_tree_found
            {
                children_tree_found = true;
                childrens_age += person.age as i16;
            }
        };
        let mut c = super::Vis::new(children_tree_finder);
        grand_parents_persons_tree.accept(&mut c);
        assert_eq!(expected_childrens_age, childrens_age);

        // The family tree age from Father
        let mut family_age: i16 = 0;
        let mut family_tree_found = false;
        let family_tree_finder = |person: &PersonType| {
            if person.notes == String::from("Father") || family_tree_found {
                family_tree_found = true;
                family_age += person.age as i16;
            }
        };
        let mut c = super::Vis::new(family_tree_finder);
        grand_parents_persons_tree.accept(&mut c);
        assert_eq!(expected_family_age, family_age);

        // The family tree age from Mother or Father
        family_age = 0;
        family_tree_found = false;
        let family_tree_finder = |person: &PersonType| {
            if person.notes == String::from("Mother")
                || person.notes == String::from("Father")
                || family_tree_found
            {
                family_tree_found = true;
                family_age += person.age as i16;
            }
        };
        let mut c = super::Vis::new(family_tree_finder);
        grand_parents_persons_tree.accept(&mut c);
        assert_eq!(expected_family_age, family_age);

        //The grand family tree age
        let mut grand_family_age: i16 = 0;
        let grand_family_tree_finder = |person: &PersonType| {
            grand_family_age += person.age as i16;
        };
        let mut gf = super::Vis::new(grand_family_tree_finder);
        grand_parents_persons_tree.accept(&mut gf);
        assert_eq!(expected_grand_family_age, grand_family_age);
    }

    #[test]
    #[should_panic]
    fn test_person_aboreal_branch_empty_contract() {
        let a = vec![];
        super::ArborealBranchType::<PersonType>::branch_from_items(a);
    }
}
