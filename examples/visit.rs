//! Examples of the use of the the visitor on an aboreal branch.
 
extern crate patterns;
use patterns::Visitable;
use std::fmt;

//A person struct to populate the tree
#[derive(Debug)]
pub struct PersonType {
    age: i8,
    title: String,
    first_name: String,
    last_name: String,
    notes: String
}

impl fmt::Display for PersonType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, r#"Title: {}\\n Name : {}, {}\\n, Age: {}\\n Patient Notes: {}\\n "#, self.title, self.last_name, self.first_name, self.age, self.notes)
    }
}

fn person_tree_example() -> String {

    let test_person_1 = PersonType{age: 22,
        title: String::from("Mr"),
        first_name: String::from("Fred"),
        last_name: String::from("Smith"),
        notes: String::from("Fred Smith has got a lot better.")};

    let mut state: String = String::new();

    let state_closure = |person: &PersonType| -> () {
        state.push_str(&format!("{} {} {}\nAge: {}\nNotes: {}\n",  person.title, person.first_name, person.last_name, person.age, person.notes));
    };

    let b = patterns::ArborealBranchType::<PersonType>::branch_from_item(test_person_1);
    let mut v = patterns::Vis::new(state_closure);
    b.accept(&mut v);
    state
}


fn multi_person_example() -> String {

    let test_person_1 = PersonType{age: 22,
                                   title: String::from("Mr"),
                                   first_name: String::from("Fred"),
                                   last_name: String::from("Smith"),
                                   notes: String::from("Fred Smith has got a lot better.")};

    let test_person_2 = PersonType{age: 33,
                                   title: String::from("Ms"),
                                   first_name: String::from("Nora"),
                                   last_name: String::from("Jones"),
                                   notes: String::from("Nora Jones is getting better.")};

    let test_person_3 = PersonType{age: 33,
                                   title: String::from("Mr"),
                                   first_name: String::from("Sid"),
                                   last_name: String::from("Vicious"),
                                   notes: String::from("Sid Vicious is a bad man.")};

    let mut state: String = String::new();

    let state_closure = |person: &PersonType| -> () {
        state.push_str(&format!("{} {} {}\nAge: {}\nNotes: {}\n====\n",  person.title, person.first_name, person.last_name, person.age, person.notes));
    };

    let a = vec!(patterns::ArborealBranchType::<PersonType>::branch_from_item(test_person_2), 
                 patterns::ArborealBranchType::<PersonType>::branch_from_item(test_person_3));
    let b = patterns::ArborealBranchType::<PersonType>::branch_from_item_and_items(test_person_1,a);
    let c = vec!(b);
    let d = patterns::ArborealBranchType::<PersonType>::branch_from_items(c);


    let mut v = patterns::Vis::new(state_closure);
    d.accept(&mut v);
    state
}

fn main() {
    let person_1_report = person_tree_example();
    let multi_person_report = multi_person_example();
    println!("\nReport On Person 1");
    println!("\n============");
    println!("{}\n", person_1_report);
    println!("\n============\n");
    println!("\nReport On Multi Person Tree");
    println!("\n============");
    println!("{}\n", multi_person_report);
    println!("\n============");
}

