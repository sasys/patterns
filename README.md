# Arboreal Branch - A Simple Composite Tree Structure With Visitor

[![CircleCI](https://circleci.com/bb/sasys/patterns.svg?style=svg)](https://circleci.com/bb/sasys/patterns)

*Animals that live in the trees are called ‘arboreal’ and they have some amazing adaptations to make the most of their leafy surroundings at every level.*

A simple tree structure with a visitor.  See the examples below.

## ToDo
* [] Add in more contract clauses
* [] Implement the builder pattern for the tree
* [] Make sure the api guidlines are implements as far as possible at : https://rust-lang.github.io/api-guidelines/documentation.html
    * [] Includes documentation
    * [x] Examples
    * [] Common meta data in the toml
    * [x] Should have been arboreal branch!!
* [] The visitor code has some elements like :
    ```rust
        None    => println!("No children."),
    ```
These should be altered to do something more sensible.

## Clean Build And Test

    cargo clean
    cargo build
    cargo test

Or with output

    cargo test -- --nocapture

## Examples

This shows how to run a visitor over a tree.

    cargo run --example visit

## Documentation

    cargo doc  --no-deps --open
